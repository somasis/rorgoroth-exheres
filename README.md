---
# NOTICE: THIS PROJECT HAS BEEN ABANDONED
If you use any of these packages please submit a PR to me for removal to your repo (must be on listed on [summer](https://git.exherbo.org/summer/)).

---
### ::rorgoroth

Miscellaneous packages for the [Exherbo](http://exherbo.org/) GNU/Linux distribution.
View the repo on [Exherbo Summer](http://git.exherbo.org/summer/repositories/rorgoroth/index.html).